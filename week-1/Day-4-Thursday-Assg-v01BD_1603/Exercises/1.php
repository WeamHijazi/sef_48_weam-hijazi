#!/usr/bin/php
<?php
 $input = getopt("i:");
 $path=$input['i'];   // To read the whole input path
 if(is_dir($path))    // if the input is a correct directory
 {
 	echo "Files within ".$path.": \n";
        recursive_directory($path);  // call the function
  }
  if(!is_dir($path))   // if not typed correctly show an error message
  {
  	echo "Directory does not exist";
  }
 function recursive_directory($directory)
{
 
$iterator = new DirectoryIterator($directory);
foreach ($iterator as $fileinfo) 
{
	if (!$fileinfo->isDot())  // check "." and ".." directories
    {
    	echo $fileinfo->getFilename() . "\n";

		if(is_dir("$directory"."/"."$fileinfo"))
	{

		recursive_directory("$directory"."/"."$fileinfo");
	}

    }
} // end of for loop
} //end of function
?>

