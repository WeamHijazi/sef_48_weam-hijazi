#!/usr/bin/php
<?php
$log_file = fopen("/var/log/apache2/access.log.1", "r" ); // open the access.log file
while(!feof($log_file)) // loop untill the End of the file
 {
 $response= fgets($log_file); // reads each line in the file
  
 $arr=explode(' ',trim($response)); // split till space
 // check if we have null value to avoid the undefined offeset error
  if (!isset($arr[0]))
 {
  $arr[0]=null;
 }
  if (!isset($arr[3]))
 {
  $arr[3]=null;
 }
  if (!isset($arr[5]))
 {
  $arr[5]=null;
 }
  if (!isset($arr[6]))
 {
  $arr[6]=null;
 }
  if (!isset($arr[7]))
 {
  $arr[7]=null;
 }
  if (!isset($arr[8]))
 {
  $arr[8]=null;
 }
 $ip=$arr[0];    //ip adress
 $date_time=substr($arr[3],1);  // date & time subtract the first string []
 $GET_response=$arr[5]." ".$arr[6]." ".$arr[7]; 
 $HTTP_Code=$arr[8]; 
 
 $result=explode(':',trim($date_time));   // separate date and time by : 
 // check if we have null value to avoid the undefined offeset error
  if (!isset($result[0]))
 {
  $result[0]=null;
 }
   if (!isset($result[1]))
 {
  $result[1]=null;
 }
   if (!isset($result[2]))
 {
  $result[2]=null;
 }
   if (!isset($result[3]))
 {
  $result[3]=null;
 }
 $date=$result[0];                        // first we get time in the format day/month/year
 $hours=$result[1];   // number of hours
 $min=$result[2];    // number of mins 
 $second=$result[3];   // number of seconds
// explode the date by / to get the day, month and the year separately 
 $result2=explode('/',trim($date));  
 // check if we have null value to avoid the undefined offeset error      
   if (!isset($result2[0]))
 {
  $result2[0]=null;
 }
  if (!isset($result2[1]))
 {
  $result2[1]=null;
 }
  if (!isset($result2[2]))
 {
  $result2[2]=null;
 }
 $day=$result2[0];    // the day as a number
 $month=$result2[1];   // month as a name
 $year=$result2[2];    // year 
 // get the name of the day given the date ; l is the formate of the given date, it's the full tetual representation of a day
 $Day_Name=date('l',strtotime($date));    

 echo $ip." -- ".$Day_Name.", ".$month." ".$day." ".$year." : ".$hours."-".$min."-".$second." -- ".$GET_response." -- ".$HTTP_Code."\n";

} // End of while loop
fclose($log_file);  // close the file
?>
