<?php

namespace WeamProject\Http\Controllers;

use Illuminate\Http\Request;
use WeamProject\Post;

use WeamProject\Http\Requests;

class PostsController extends Controller
{
    //
    public function getAll() {
    $blogs = Post::all();
		foreach($blogs as $value) {
			return view('viewPosts')->with(['blogs'=>$blogs]);
			
		}
	}

	public function getMore() {
    $blogs = Post::all();
		foreach($blogs as $value) {
			return view('blogpost')->with(['blogs'=>$blogs]);
			
		}
	}

   public function showAllBlogs()
    {
    	$blogs = Post::all();
        return view('home')->with('tutu',$blogs);
    }

     public function create() {
    	return view('index');
    }

     public function store(Request $request)
 {
     $newPost = new Post();
     $newPost->title= $request->get('title');
     $newPost->description= $request->get('description');
     $newPost->UserID= $request->user()->id;
     $newPost->save();
     return redirect('/home')->withMessage('Posted Successfully');
 }

 

    public function tafjir(Request $request, $id) {
		$blogToUse = Post::find($id);
		if($blogToUse) {
			$blogToUse->delete();
			Post::destroy($id);
			$data['message'] = 'Post deleted Successfully';
		}
		else {
			$data['errors'] = 'Invalid Operation';
		}
		return redirect('/blogs')->with($data);
	}

}





    
    

   
/*
  


	public function validator(array $data)
    {
        return Validator::make($data, [
            $request->title => 'required|min:10|max:255',
            $request->title => array('Regex:/^[A-Za-z0-9 ]+$/'),
            $request->description => 'required|min:50',
        ]);
    }

	public function enterNewBlog(array $data)
    {
        return Blog::create([
            'title' => $data['title'],
            'description' => $data['description'],
        ]);
    }

	public function editOldBlog(Request $request) {
		$oldBlog = Bloggie\Blog::find(1);

		$oldBlog->title = $request->title;
		$oldBlog->description = $request->description;

		$oldBlog->save();
	}

	public function tafjir(Request $request, $id) {
		$blogToUse = Blog::find($id);
		if($blogToUse) {
			$blogToUse->delete();
			Blog::destroy($id);
			$data['message'] = 'Post deleted Successfully';
		}
		else {
			$data['errors'] = 'Invalid Operation';
		}
		return redirect('/blogs')->with($data);
	}


	// public function enterNewBlog(Request $request) {
	// 	$newBlog = new Blog;

	// 	$newBlog->title = $request->title;
	// 	$newBlog->description = $request->description;

	// 	$newBlog->save();
	// }
}