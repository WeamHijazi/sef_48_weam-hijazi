<?php

namespace WeamProject;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $fillable = ['UserID', 'title', 'description'];
}
