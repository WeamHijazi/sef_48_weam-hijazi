<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/he','PostsController@getAll');
Route::get('/blogpost/{id}','PostsController@getAll');






// Route::get("posts" , 'PostsController@getAll');
// Route::get("delete/{id}", 'PostsController@tafjir');


Route::group(['middleware' => ['auth']], function()
{
	Route::get('data/create', 'PostsController@create');
	Route::post('newPost', 'PostsController@store');
});


