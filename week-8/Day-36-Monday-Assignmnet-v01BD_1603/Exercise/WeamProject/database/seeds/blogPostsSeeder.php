<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('posts')->insert([
			['id' => 1, 'title' => "Mercedes", 'UserID' => 123544, 'description' => "Model 2001 is having problems in Electric systems"],
			['id' => 2, 'title' => "BMW", 'UserID' => 123421, 'description' => "Model 2011 is having problems in Electric systems"],
			['id' => 3, 'title' => "Toyota", 'UserID' => 123500, 'description' => "Model 2013 is having problems in Electric systems"],
			['id' => 4, 'title' => "MiniCooper", 'UserID' => 122544, 'description' => "Model 2003 is having problems in Electric systems"],
		]);
    }
}
