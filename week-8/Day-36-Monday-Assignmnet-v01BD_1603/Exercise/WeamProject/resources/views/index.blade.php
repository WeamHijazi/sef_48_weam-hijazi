@extends('layouts.app')
@section('content')

<div class="panel-body">
<form class="form-horizontal" role="form" method="POST" action="{{url('newPost')}}">
    {{ csrf_field() }}
  <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
  <label for="title" class="col-md-4 control-label">Add New Post</label>
  <br>
  <br>
    <div class="col-md-6">
      <input id="title" type="text" class="form-control" placeholder="type your title" name="title"required autofocus>
    </div>
    <br>
    <br>
    <br>
    <div class="col-md-6">
      <textarea id="description" type="text" class="form-control" placeholder="type your description" name="description"required autofocus></textarea> 
    </div>
    
    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
         <br>
        <button type="submit" class="btn btn-primary">
          Submit
        </button>
      </div>
     </div>
</form>
</div>

@endsection