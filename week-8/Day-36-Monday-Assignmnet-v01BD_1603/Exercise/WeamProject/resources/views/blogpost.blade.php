@extends('layouts.app')@section('content')
<div class="container">
   <div class="row">
       <div class="col-md-8 col-md-offset-2">
           <div class="panel panel-default">
                  <div class="panel-heading">blogposts number {!! $blog['id']!!}
                  <div class="panel-body">
                    <div>
                        {!! $blog['title']!!}<br>
                        {!! $blog['description']!!}
                    </div>
                     </div>
                     </div>
      
           </div>
       </div>
   </div>
</div>
@endsection