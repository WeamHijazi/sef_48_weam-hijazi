<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body>
    
        <script src="js/main.js"></script>
        <div id="mainApp">
            <h1> SEF Todo List </h1>
            <div id="Icontainer">
                <form name="form" action="" method="POST">
                <div><label id="label"> Item  </label></div>
                <input id="Item" type="text" name="Item" placeholder="Todo title" required />
                <button type="button" onclick="AddItem()">Add</button>
            </div>
            <div>
                <textarea id="Description" placeholder="Description" required /></textarea>
            </div>
                </form> 
            <div id="line" class="separator"></div>
        </div>
    </body>
</html>

