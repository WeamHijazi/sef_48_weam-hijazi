

function AddItem() 
{  
	
	// save the input values in two variables
	var Item= document.getElementById("Item").value;
	var Description=document.getElementById("Description").value;
	// rest the input to avoid adding empty Items
	form.reset();
	// get the Current date when the Item was added
	var today=getCurrentDate();
	
	// count number of Items Added and assign it to the id of the Item
	var Itemscount=document.getElementsByClassName("AddedItem");
	var ItemID=Itemscount.length;
	//alert(ItemID);

	// check if any of the input is empty
	if((Item=="")||(Description=="")){
		alert("Input cannot be empty");
	}else{
		
		// create the main container of the Item
		var ItemTobeAdded = document.createElement("div");
		ItemTobeAdded.id = ItemID;
		ItemTobeAdded.className = "AddedItem";
		ItemTobeAdded.style.width="550px";
		ItemTobeAdded.style.height="150px";
		ItemTobeAdded.style.border="2px solid #808080";
		ItemTobeAdded.style.borderRadius="5px";
		ItemTobeAdded.style.margin="20px ";
		document.getElementById("mainApp").appendChild(ItemTobeAdded);

		// Create Container for text
		var ItemContent = document.createElement("div");
		ItemContent.id = "ItemArea";
		ItemContent.style.width="85%";
		ItemContent.style.height="120px";
		ItemContent.style.borderRight="2px solid #808080";
		ItemContent.style.marginTop="15px";
		ItemContent.style.marginRight="15px";
		ItemContent.style.display="block";
		ItemContent.style.paddingLeft="10px";
		ItemContent.style.marginBottom="15px";
		document.getElementById(ItemID).appendChild(ItemContent);

		//Create internal boxes for text input
		ItemContent.innerHTML='<h2 id="ItemTitle"><b></b></h2>\
								<p id="Date" ></p>\
								<h4 id="ItemDesc"></h4>';

		// fill the information in the correct box
		
		// document.getElementById("ItemTitle").innerHTML=Item;
		// document.getElementById("ItemDesc").innerHTML=Description;
		// document.getElementById("Date").innerHTML="added: "+today;
		
		document.getElementById(this.parentNode.parentNode.id).getElementById("ItemTitle").innerHTML=Item;
		document.getElementById(this.parentNode.parentNode.id).getElementById("ItemDesc").innerHTML=Description;
		document.getElementById(this.parentNode.parentNode.id).getElementById("Date").innerHTML="added: "+today;

		// Create container for delete Button
		var Delete = document.createElement("div");
		Delete.id = "DeleteBox";
		Delete.style.width = "15%";
		Delete.style.height = "100%";
		Delete.style.marginTop = "-100px";
		Delete.style.marginRight="-20px";
		Delete.style.display ="block";
		Delete.style.float="right";
		//Delete.style.display="inline";
		document.getElementById(ItemID).appendChild(Delete);
		Delete.innerHTML='<input type="button" id="Delete" value="X" onclick="DeleteItem(this)">';
	}
	
};

function getCurrentDate()
{
	var now= new Date();
	var mm=now.getMonth()+1; // January is 0
	var yyyy=now.getFullYear();
	var hr = now.getHours();
	var min = now.getMinutes();
	var days= ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var months=['January','February','March','April','May','June','July','August','September','October','November','December'];
	var day=days[now.getDay()];
	var month=months[now.getMonth()];
	var today = day+', '+month+' '+now.getDay()+' '+yyyy+' '+hr+':'+min;
	return today;
	
}



function DeleteItem(element)
{
	//console.log(this.id);
	//var element = this;
	//console.log(element.parentNode.parentNode.id);
	var mainApp = document.getElementById("mainApp");
    mainApp.removeChild(element.parentNode.parentNode);
}