<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TagsQuestions', function (Blueprint $table) {
            $table->integer('Question_id')->unsigned();
            $table->foreign('Question_id')
                  ->references('id')->on('Questions')
                  ->onDelete('cascade');
            $table->integer('Tag_id')->unsigned();
            $table->foreign('Tag_id')
                  ->references('id')->on('Tags')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TagsQuestions');
    }
}
