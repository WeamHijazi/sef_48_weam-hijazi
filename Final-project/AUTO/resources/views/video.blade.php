@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-16 ">
            <div id="Icontainer">
                <form name="form" action="{{url('/video/AddYoutube')}}" method="POST">
                      
                      <input name="_token" id="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div>
                        <label id="label2"><b>Add Youtube Video</b></label>
                    </div>
                    <input id="Youtubelink" type="url" name="Youtubelink"  placeholder="YOUTube link" required />
                    <button type="submit">Submit</button>
                </form>
            </div>

            <script type="text/javascript">
                function isUrlValid(userInput) {
                            var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
                     if(res == null)
                                return false;

                                else

                                return true;

                            }

videos = document.querySelectorAll("video");
for (var i = 0, l = videos.length; i < l; i++) {
    var video = videos[i];
    var src = video.src || (function () {
        var sources = video.querySelectorAll("source");
        for (var j = 0, sl = sources.length; j < sl; j++) {
            var source = sources[j];
            var type = source.type;
            var isMp4 = type.indexOf("mp4") != -1;
            if (isMp4) return source.src;
        }
        return null;
    })();
    if (src) {
        var isYoutube = src && src.match(/(?:youtu|youtube)(?:\.com|\.be)\/([\w\W]+)/i);
        if (isYoutube) {
            var id = isYoutube[1].match(/watch\?v=|[\w\W]+/gi);
            id = (id.length > 1) ? id.splice(1) : id;
            id = id.toString();
            var mp4url = "http://www.youtubeinmp4.com/redirect.php?video=";
            video.src = mp4url + id;
        }
    }
}
            </script>
            
           
            <div class="panel panel-default PanelPosition">
                <div class="panel-heading"><b>Videos</b></div>
                    <div class="panel-body">
                    <div id="innerYoutube">

                        @foreach($youtubes as $youtube)
                            <iframe class="youtubeShow" width="300" height="250" src="https://www.youtube.com/embed/{!!$youtube['link']!!}" frameborder="0" allowfullscreen></iframe>
                         @endforeach
                    </div>
                <div class="pagination colorYouTube"> {{ $youtubes->links() }} </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</div>
@endsection
