@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-16 ">
          <div id="AskQuestion">
             <form name="form" action="{{url('/main/CreateQuestion')}}" method="POST">
    <input name="_token" id="_token" type="hidden" value="{{csrf_token()}}"/>
                    <div id="Icontainer">
                        <div><label id="label"><b><font size="3">Have A Question?</font></b></label></div>
                        <input id="Question" type="text" name="Question"  placeholder="Question title" required />
                    
                    </div>
                    <div>
                        <textarea id="Description" name="Description" placeholder="Description" required /></textarea>
                    </div>
                   
                    <a id="AddImageIcon" data-toggle="modal" data-target="#myImageModal" href="#myImageModal" class="addthis_button_compact">
                        <img src="http://s7.addthis.com/static/btn/sm-plus.gif" width="16" height="16" border="0" alt="Share" />
                    </a>
                    <p id="AddImage"> Add Image </p>
                    <a id="AddVideoIcon" data-toggle="modal" data-target="#myVideoModal" href="#myVideoModal" class="addthis_button_compact">
                        <img src="http://s7.addthis.com/static/btn/sm-plus.gif" width="16" height="16" border="0" alt="Share" />
                    </a>
                    <p id="AddVideo"> Add Video </p>
                    <button type="submit" >ASK</button>
                </form>
            </div>
            <div id="Some">
                <div class="Recommended"><font size="3"><strong> 
                TOP 5 Tags </strong></font></div>                 
                <h6 class="Tag">
                    <a href="https://www.toyota.ie/index?gclid=CNrBse3K6c8CFUZz2wod8mwNnQ">Toyota</a>
                </h6>
                <br>
                <h6 class="Tag">
                    <a href="https://www.mini.co.uk/en_GB/home.html">Mini Cooper</a>
                </h6>
                <br>
                <h6 class="Tag">
                    <a href="http://www.mercedes-benz.ie/content/ireland/mpc/mpc_ireland_website/enng/home_mpc/passengercars.html">Mercedes Benz</a>
                </h6>
                <br>                
                <h6 class="Tag">
                    <a href="http://www.bmw.ie/en/home.html">BMW</a>
                </h6>
                <br>
                <h6 class="Tag">
                    <a href="http://www.mazda.ie/?s3campaign=MMIE_MWP_Mazda2016_FY151&s3advertiser=MazdaBrandGeneric&s3banner=search">Mazda</a>
                </h6>
                <br>
            </div>
            <div class="panel panel-default PanelPosition">

                <div class="panel-heading"><b><font size="4">
                 Search Results</font></b></div>
 @if (count($questions))
                <div class="panel-body">
                    <div id="listedQuestions">
                  
                    
                        @foreach($questions as $question)
                        <div class="Q">
                            <div class="number_of_Answers">
                                 <font size="3"> <p>Answers</p></font>
                                <font size="3"> 
                                    {{count($question->answers)}} 
                                </font>                            
                            </div>
                            <div class="number_of_Views">
                                <font size="3"><p>Views</p></font>
                               
                                <font size="3">
                                    {!!$question->number_of_views!!}
                                </font>
                                 
                            </div>
                            <div class="right_part">
                                <div class="Question_title">
                                    <font size="4"><a href=" {{url('/main/Question/' . $question->id)}}">{!!$question->Question_title!!} </a></font>
                                </div>
                                <div class="AdditionalInfo">
                                    <h6 class="Tag">
                                        <a href="">Mercedes Benz</a>
                                    </h6>
                                    
                                    <h6 class="Tag">
                                        <a href="">Toyota</a>
                                    </h6>
                                    <h6 class="Tag">
                                        <a href="">Mini Cooper</a>
                                    </h6>

                                    
                                    <div class="timeAsked">
                                        <h5 class="float"><p>Asked</p></h5>
                                        <h6 class="float">
                                            <p>
<?php
    $createdAtDate=date('Y-m-d H:i:s' , strtotime($question->created_at));
    $now = new DateTime();
    $currentDAte=$now->getTimestamp(); // in value
    $createdtime=strtotime($createdAtDate);
    $time=$currentDAte-strtotime($createdAtDate);
                
    $minutesT = round(abs($time) / 60,2);
    $hours=round($minutesT/60);
    $days=round($hours/24);
    $minutes=abs($minutesT) % 60;
    $seconds = abs($time) % 60;
    if($hours>24)
    {
        echo "$days days ago";
    }
    else{
        if($minutesT<=60)
            echo "$minutes m, $seconds s ago";
        else echo "$hours h,$minutes m ago";
    }
        
?>
                                            </p>
                                        </h6>
                                       
                                        <h4 class="float"><a href="">{{ $question->user->name }}</a></h4>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="Hline"></div>
                        @endforeach
                        <div class=" colorYouTube"> {{ $questions->links() }}
                            </div>
                    </div>
                     @else 
                        <div class="Q Q2">
                        <b>Your search did not match any questions!!!<br>Post your question please!!</b>

                        </div>

                    @endif

                    <div class="vertical_line"></div>

                    <div class="rightSide">
                    
                        <iframe class="youtube" width="280" height="180" src="https://www.youtube.com/embed/vjHGY-ovsHI" frameborder="0" allowfullscreen></iframe>
                        <iframe class="youtube" width="280" height="180" src="https://www.youtube.com/embed/vR4D4bCffeI" frameborder="0" allowfullscreen></iframe>
                        <iframe class="youtube"     width="280" height="180" src="https://www.youtube.com/embed/TmxKYvIvAU0" frameborder="0" allowfullscreen></iframe>
                         <iframe class="youtube"    width="280" height="180" src="https://www.youtube.com/embed/tLJhIAobgLo" frameborder="0" allowfullscreen></iframe>
                        <a href="{{ url('/video') }}"   class="dark-button">View More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



    <!-- MODAL FOR Image -->
    <!-- Modal -->
<div class="modal fade" id="myImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add A New Photo</h4>
        </div>
        <div class="modal-body">
            <div class="row centered">
                <form  method="POST" role="form"  enctype="multipart/form-data" action="{{url('/main/CreateQuestion/image') }}">
                        {{ csrf_field() }}
                    <b>Go To Gallery:</b>
                    <input type="file" name="image" id="image"><br>
                    <button type="submit" >Add</button>
                </form>
            </div>  
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <!-- MODAL FOR Video -->
    <!-- Modal -->
<div class="modal fade" id="myVideoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add A New Video</h4>
        </div>
        <div class="modal-body">
            <div class="row centered">
                <form  method="POST" role="form"  enctype="multipart/form-data" action="{{url('/main/CreateQuestion/video') }}">
                        {{ csrf_field() }}
                    <b>Go To Gallery:</b>
                    <input type="file" name="video" id="video"><br>
                    <button type="submit" >Add</button>
                </form>
            </div>  
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="{{ url('/js/incrementViews.js') }}"></script>
@endsection
