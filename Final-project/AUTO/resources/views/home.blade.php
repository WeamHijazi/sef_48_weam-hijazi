@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            
                <div id="main-wrapper">
                    <div class="inner-wrapper header-section-bg">
                        <section id="header-section" class="inner-section">
                    <!-- LOGO -->
                           
                            <!-- HEADER MENU -->
                            <div id="header-menu">
                                <ul class="inline-menu">
                                    <li><a href="#" class="menu-item-active">HOME</a></li>
                                    <li><a href="#">ABOUT</a></li>
                                    <li><a href="#">EXPERTISE</a></li>
                                    <li><a href="#">TEAMS</a></li>
                                    <li><a href="#">WORKS</a></li>
                                    <li><a href="#">CONTACT</a></li>
                                </ul>
                            </div>
                            <!-- HEADER TITLE -->
                            <div id="header-title">
                               
                                <!-- SEPARATOR -->
                                <div class="separator"></div>
                             
                               
                            </div>
                            <!-- HEADER TEXT -->
                            <div id="header-text">
                                <font size="6"><p style="color:#FF4500;">Auto-Runners</p>,<p> A platform that connects car manufacturers, resellers, mechanics, and owners, to exchange knowledge about troubleshooting and latest technologies!!!</p></font>
                            </div>
                            <!-- HEADER FOOTER -->
                            
                        </section>
                    </div>
                </div>  
           
        </div>
    </div>
</div>
<div class="empty"> </div>
<div class="empty"> </div>
<div class="empty"> </div>
@endsection
