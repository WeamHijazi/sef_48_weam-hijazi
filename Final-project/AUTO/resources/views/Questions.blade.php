@extends('layouts.app')

@section('content')
<div class="QuestionInner">
    <div id="AskQuestion">
        <form name="form" action="{{url('/main/CreateQuestion')}}" method="POST">
            <input name="_token" id="_token" type="hidden" value="{{csrf_token()}}"/>
            <div id="Icontainer">
                <div>
                    <label id="label"><b>Have A Question?</b></label>
                </div>
                <input id="Question" type="text" name="Question"  placeholder="Question title" required />
            </div>
            <div>
                <textarea id="Description" name="Description" placeholder="Description" required /></textarea>
            </div>
            <a id="AddImageIcon" data-toggle="modal" data-target="#myImageModal" href="#myImageModal" class="addthis_button_compact">
                <img src="http://s7.addthis.com/static/btn/sm-plus.gif" width="16" height="16" border="0" alt="Share" />
            </a>
                <p id="AddImage"> Add Image </p>
            <a id="AddVideoIcon" data-toggle="modal" data-target="#myVideoModal" href="#myVideoModal" class="addthis_button_compact">
                <img src="http://s7.addthis.com/static/btn/sm-plus.gif" width="16" height="16" border="0" alt="Share" />
            </a>
                <p id="AddVideo"> Add Video </p>
            <button type="submit" >ASK</button>
        </form>
    </div>
    <div id="Some">
    
                <div class="Recommended"><font size="3"><strong> 
                TOP 5 Tags </strong></font></div>                
                <h6 class="Tag">
                    <a href="https://www.toyota.ie/index?gclid=CNrBse3K6c8CFUZz2wod8mwNnQ">Toyota</a>
                </h6>
                <br>
                <h6 class="Tag">
                    <a href="https://www.mini.co.uk/en_GB/home.html">Mini Cooper</a>
                </h6>
                <br>
                <h6 class="Tag">
                    <a href="http://www.mercedes-benz.ie/content/ireland/mpc/mpc_ireland_website/enng/home_mpc/passengercars.html">Mercedes Benz</a>
                </h6>
                <br>                
                <h6 class="Tag">
                    <a href="http://www.bmw.ie/en/home.html">BMW</a>
                </h6>
                <br>
                <h6 class="Tag">
                    <a href="http://www.mazda.ie/?s3campaign=MMIE_MWP_Mazda2016_FY151&s3advertiser=MazdaBrandGeneric&s3banner=search">Mazda</a>
                </h6>
                <br>
            
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default PanelPosition">
                    <div class="panel-heading"></div>
                    <div class="innerQ">
              
                        
                        <div class="panel-body">

                            <div id="QuestionDetail">
                               <font size="5"><p style="color:#000;"> {!! $question->Question_title !!}</p></font>
                                
                                 <a href="">
                                    {{ $question->user->name }}
                                </a>
                                @if($question->image_url)
                                <div class="ImageBox">
                                    <div class="innerImage">
                                        <img src="{!!url('/uploads/'.$question->image_url)!!}" alt="image was uploaded">
                                    </div>
                                </div>
                                        @endif
                                {!!$question->Question_text!!}
                                
                                
                            </div>
                        
                            

                            @if (count($question->Answers))
                            <div id="AllAnswers"> 
                                @foreach($question->Answers as $Answer)
                                    <div class="AnswersDiv">
                                        <a href="">
                                            {{ $Answer->user->name }}
                                        </a>
                                        <br>
                                         @if($Answer->image_url)
                                         <div class="ImageBox">
                                            <div class="innerImage">
                                                <img src="{!!url('/uploads/'.$Answer->image_url)!!}" alt="image was uploaded">
                                            </div>
                                        </div>
                                        @endif
                                        {{ $Answer->Answer_text }}
                                        
                                    </div>
                                    
                                @endforeach
                            </div> 
                            @else 
                                No Answer yet

                            @endif
                        </div>
                    </div>

                    <div>
                        <form name="form" action="{{url('/main/Question/'. $question->id.'/CreateAnswer')}}" method="POST">
                            <input name="_token" id="_token" type="hidden" value="{{csrf_token()}}"/>
                            <label id="label"><b>Add Your Answer Here</b></label>
                            <input name="id" id="{{$question->id}}" type="hidden" value="{{$question->id}}"/>
                            <textarea id="Answer" name="Answer" placeholder="Answer" required /></textarea>
                            <a id="AddImageIcon" data-toggle="modal" data-target="#myImageModal" href="#myImageModal" class="addthis_button_compact">
                            <img src="http://s7.addthis.com/static/btn/sm-plus.gif" width="16" height="16" border="0" alt="Share" />
                            </a>
                            <p id="AddImage"> Add Image </p>
                            <a id="AddVideoIcon" data-toggle="modal" data-target="#myVideoModal" href="#myVideoModal" class="addthis_button_compact">
                            <img src="http://s7.addthis.com/static/btn/sm-plus.gif" width="16" height="16" border="0" alt="Share" />
                            </a>
                            <p id="AddVideo"> Add Video </p>
                            <button type="submit"> Answer</button>
                        </form>
                    </div>
           
                </div>    
            </div>
            <div class="rightSide2">
                <div class="Recommended"><font size="3"><strong> Recommended for You </strong></font></div>
                 <iframe class="youtube" width="280" height="180" src="https://www.youtube.com/embed/vjHGY-ovsHI" frameborder="0" allowfullscreen></iframe>
                <iframe class="youtube" width="280" height="180" src="https://www.youtube.com/embed/FnGOSJKltIw" frameborder="0" allowfullscreen></iframe>
                <iframe class="youtube"     width="280" height="180" src="https://www.youtube.com/embed/0DS9PY6iaxE" frameborder="0" allowfullscreen></iframe>
                <a href="{{ url('/video') }}"   class="dark-button">View More</a>
            </div>
        </div>
    </div>
</div>


    <!-- MODAL FOR Image -->
    <!-- Modal -->
<div class="modal fade" id="myImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add A New Photo</h4>
        </div>
        <div class="modal-body">
            <div class="row centered">
                <form  method="POST" role="form"  enctype="multipart/form-data" action="{{url('/main/CreateQuestion/image') }}">
                        {{ csrf_field() }}
                    <b>Go To Gallery:</b>
                    <input type="file" name="image" id="image"><br>
                    <button type="submit" >Add</button>
                </form>
            </div>  
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <!-- MODAL FOR Video -->
    <!-- Modal -->
<div class="modal fade" id="myVideoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Add A New Video</h4>
        </div>
        <div class="modal-body">
            <div class="row centered">
                <form  method="POST" role="form"  enctype="multipart/form-data" action="{{url('/main/CreateQuestion/video') }}">
                        {{ csrf_field() }}
                    <b>Go To Gallery:</b>
                    <input type="file" name="video" id="video"><br>
                    <button type="submit" >Add</button>
                </form>
            </div>  
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
