<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/main', 'MainController@index');
Route::get('/main', 'MainController@GetRecentQuestion');
Route::get('/video', 'YoutubeController@GetAllVideos');
Route::get('/main/Question/{id}', 'QuestionController@index');

Route::post('/main/search','MainController@search');
Route::get('/main/search','MainController@search');





 Route::group(['middleware' => ['auth']], function()
{
route::post('/main/CreateQuestion','MainController@CreateQuestion');
route::get('/main/CreateQuestion','MainController@GetRecentQuestion');

route::post('/main/CreateQuestion/image','MainController@AddImage');
route::get('/main/CreateQuestion/image','MainController@GetRecentQuestion');

route::post('/main/CreateQuestion/video','MainController@AddVideo');
route::get('/main/CreateQuestion/video','MainController@GetRecentQuestion');




route::post('/main/Question/{id}/CreateAnswer','QuestionController@addAnswer');
route::get('/main/Question/{id}/CreateAnswer','QuestionController@index');

route::post('/video/AddYoutube','YoutubeController@AddYoutube');
route::get('/video/AddYoutube','YoutubeController@viewYoutube');
	

 });

