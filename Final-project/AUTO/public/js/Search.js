var searchId;

$(document).ready(function()
{

  $('#searchInput').on('input',function(e)
  {
    var sugBox =  $(this).siblings('#suggestionBox');
    sugBox.show();
    var textToSearch = $("#searchInput").val();
    if(textToSearch == ""){
      sugBox.hide();
      return;
    }
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    })
    var dataToSend = {
      'text': textToSearch,
    }
    $.ajax({
      type:'POST',
      url:'/main/search',
      contentType: "json",
      jsonp: false,
      processData: false,
      data:JSON.stringify(dataToSend),
      success:function(data){

        $("#suggestionBox").html("");
        var results = data.results;
        for(var i =0; i < results.length; i++) {
          var itemToAdd = "<button class =\"city-result col-md-10 col-md-offset-3 btn btn-default\" data-id=\""+results[i].id+"\" > "+
                     +
                     " <span> " + results[i].Question_title + " </span> "+
                 " </div> " +
                " </button>";
          $("#suggestionBox").append(itemToAdd);
        }
        
      }, error:function(error) {
         console.log("this is failing to search "+ dataToSend['text']);
         },
      });
  });

  $(document).on('click', "button.city-result", function(e) {

    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    })

    searchId = $('button.city-result').attr('data-id');

    var dataToSend = {
      'text': searchId,
    }
    e.preventDefault();
    $.ajax({
      type:'POST',
      url:'/main/Question/{id}',
      // contentType: "json",
      // processData: false,
      jsonp: false,
      data:JSON.stringify(dataToSend),

      success:function(data){
        
        $('#cityHere').html(data);
        console.log("Search ID is "+searchId);
        checkWeather(searchId);
        fillItem(searchId);
        $("#suggestionBox").html("");
            
      }, error:function(error) {
           console.log(error);
          },
    });
  });

});