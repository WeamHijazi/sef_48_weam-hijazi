<?php

namespace AUTO;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //
    public $table = "Votes";
}
    public function user(){
    	return $this->belongsTo('AUTO\User');
	}

	public function answer() {
        return $this->belongsTo('AUTO\Answer');
    }
}
