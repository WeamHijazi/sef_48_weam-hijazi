<?php

namespace AUTO;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $table = "Questions";
    public function user() 
   {
        return $this->belongsTo('AUTO\User');
   }


    public function answers(){
    	return $this->hasMany('AUTO\Answer', 'Question_id', 'id');
	}
	
   public function tag(){
      return $this->hasMany('AUTO\TagQuestion', 'Question_id', 'id');
  }
	
}
