<?php

namespace AUTO;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use AUTO\Answer;
use AUTO\Tag;

class User extends Authenticatable
{
    use Notifiable;

     public function Answers(){
        return $this->hasMany('AUTO\Answer');
    }

    public function Tags(){
        return $this->hasMany('AUTO\Tag');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
