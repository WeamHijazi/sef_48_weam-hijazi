<?php

namespace AUTO;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    protected $table = "Answers";
    public function Question() {
        return $this->belongsTo('AUTO\Question');
    }

    public function user(){
    	return $this->belongsTo('AUTO\User');
	}
}
