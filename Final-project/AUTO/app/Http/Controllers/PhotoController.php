<?php

namespace AUTO\Http\Controllers;

use Illuminate\Http\Request;

use AUTO\Http\Requests;

class PhotoController extends Controller
{
    //
     public function AddImage (Request $request)
    {   
        // echo csrf_token();
        // e5AoBt9E6mIFsAAyKwQLcXVJL4864X4g3GFV7eoF
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg',]);
        $image = $request->file('image');
        
        $imageName = csrf_token().'_'.$request->user()->id.'_'.$image->getClientOriginalExtension();
        $destinationPath = public_path('uploads');
        $image->move($destinationPath, $imageName);

        return back()->with('success','Question Upload successful',$imageName);
    }

    public function AddVideo (Request $request)
    {

         $this->validate($request, ['video' => 'required|video|mimes:3gp,MPEG-4,Flash Lite',]);
        $video = $request->file('video');
        $videoName = time().'_'.$request->user()->id.'_'.$video->getClientOriginalExtension();
        $destinationPath = public_path('uploads');
        $video->move($destinationPath, $videoName);
        return back()->with('success','Question Upload successful');
    }

}
