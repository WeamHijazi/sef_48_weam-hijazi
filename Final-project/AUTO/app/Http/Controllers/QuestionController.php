<?php

namespace AUTO\Http\Controllers;

use Illuminate\Http\Request;

use AUTO\Http\Requests;
use AUTO\Question;
use AUTO\Answer;
use Auth;

class QuestionController extends Controller
{
    //
    public function Create(Request $request){
    	
       
    }

     public function index($id)
    { 
        $Answers=Answer::get();
        $questions=Question::where('id',$id)->first();
        $questions->number_of_views = $questions->number_of_views + 1;
        $questions->save();
        //echo $questions->number_of_views;
        //echo $questions;
         return view('Questions')->with('question',$questions)
                                 ->with('Answers',$Answers); 
    }
  //
  


     public function addAnswer(Request $request) {
        
        $newAnswer = new Answer();
        $newAnswer->Answer_text = $request->get('Answer');
        $newAnswer->Question_id = $request->get('id');
        $newAnswer->user_id = Auth::id();
        $newAnswer->image_url=csrf_token().'_'.$request->user()->id.'_'.'jpeg';
  		//$newAnswer->Number_of_votes = 0;
        $newAnswer->save();
        return back()->with('success','Answer Upload successful');
    }

}
