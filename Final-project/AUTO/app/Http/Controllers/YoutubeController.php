<?php

namespace AUTO\Http\Controllers;

use Illuminate\Http\Request;

use AUTO\Http\Requests;

use AUTO\Youtube;

class YoutubeController extends Controller
{
    //
     public function index()
    {
        return view('video');
    }

    public function AddYoutube(Request $request)
    {  
        $link=$request->get('Youtubelink');
        $link_explode=explode("=", $link);
        $checklink=$this->GetAllYoutube($link_explode[1]);
        if ($checklink<1){
            echo "empty";
    	    $newYoutube = new Youtube();
            
    	    $newYoutube->link=$link_explode[1];
    	    $newYoutube->user_id = $request->user()->id;
    	    $newYoutube->save();
            return back()->with('success','Question Upload successful');
        }else echo "already exist";
    }

    public function viewYoutube()
    {
        return view('video');
    }

    public function GetAllYoutube($link)
    {
       
         $youtubes=Youtube::where('link',$link)->count();
         return $youtubes;
    }

    public function GetAllVideos()
    {
        $youtubes=Youtube::orderBy('created_at', 'DESC')->paginate(9);
        return view('video')->with('youtubes',$youtubes);
    }



}
