<?php

namespace AUTO\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use AUTO\Http\Requests;

use AUTO\Question;
use AUTO\Answer;

class MainController extends Controller
{
    //
    public function index()
    {
        return view('main');
    }


    public function CreateQuestion (Request $request)
    {
        // echo csrf_token();
        // e5AoBt9E6mIFsAAyKwQLcXVJL4864X4g3GFV7eoF
       
    	$newQuestion = new Question();
    	$newQuestion->Question_title=$request->get('Question');
        $newQuestion->Question_text=$request->get('Description');
        $newQuestion->image_url=csrf_token().'_'.$request->user()->id.'_'.'jpeg';
        $newQuestion->user_id = $request->user()->id;
        $newQuestion->save();
        return back()->with('success','Question Upload successful');
        redirect('/main');
    }

       public function AddImage (Request $request)
    {   
        // echo csrf_token();
        // e5AoBt9E6mIFsAAyKwQLcXVJL4864X4g3GFV7eoF
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg',]);
        $image = $request->file('image');
        
        $imageName = csrf_token().'_'.$request->user()->id.'_'.$image->getClientOriginalExtension();
        $destinationPath = public_path('uploads');
        $image->move($destinationPath, $imageName);

        return back()->with('success','Question Upload successful',$imageName);
    }

    public function AddVideo (Request $request)
    {

         $this->validate($request, ['video' => 'required|video|mimes:3gp,MPEG-4,Flash Lite',]);
        $video = $request->file('video');
        $videoName = time().'_'.$request->user()->id.'_'.$video->getClientOriginalExtension();
        $destinationPath = public_path('uploads');
        $video->move($destinationPath, $videoName);
        return back()->with('success','Question Upload successful');
    }

   
    public function GetRecentQuestion()
    {
        
        // $questions = DB::table('Questions')
        //         //->join('Answers', 'Answers.Question_id', '=', 'Questions.id')
        //         ->orderBy('Questions.created_at', 'DESC')
        //         ->paginate(13);
        // return view('main')->with('questions',$questions)
        //                    ->with('Answers',$Answers); 
        //                    //echo $Answers; 
        $questions=Question::orderBy('Questions.created_at', 'DESC')
                      ->with('answers')
                     ->paginate(10);
    return view('main')->with('questions',$questions);

                          
    }

    public function CountAnswers($id)
    {
        $questions=Question::get();
          $Answers=Answer::where('Question_id',$id)->count();
          return $Answers;

    }

    

     public function search(Request $request)
    {
         $txt = $request->get('SearchInput');
        $results = Question::where('Question_title', 'LIKE', '%'.$txt.'%')
                            ->orderBy('Questions.created_at', 'DESC')
                            ->with('answers')
                            ->paginate(10);
       
    return view('search')->with('questions',$results);
    }


}
