<?php

namespace AUTO;

use Illuminate\Database\Eloquent\Model;

class TagQuestion extends Model
{
    //
    protected $table = "TagsQuestions";
    public function Question() {
        return $this->belongsTo('AUTO\Question');
    }
}
