-- SBQ-1
SELECT 
    CONCAT(A.first_name, ' ', A.last_name) AS 'Actor Name',
    COUNT(FA.film_id) AS '# of Movies'
FROM
    actor AS A
        JOIN
    film_actor AS FA ON A.actor_id = FA.actor_id
GROUP BY A.actor_id;

-- SBQ-2
SELECT 
    L.name, T.MCount
FROM
    language AS L
        JOIN
    (SELECT 
        F.language_id, COUNT(F.language_id) AS MCount
    FROM
        film AS F
    GROUP BY F.language_id
    ORDER BY MCount DESC) AS T ON L.language_id = T.language_id;
    
-- SBQ-3
SELECT 
    CTR.country, COUNT(CTR.country_id) as CustomNumber
FROM
    customer AS C
        JOIN
    address AS AD ON C.address_id = AD.address_id
    JOIN city AS CT ON AD.city_id = CT.city_id
    JOIN country AS CTR ON CTR.country_id = CT.country_id
    GROUP BY CTR.country_id
    ORDER BY CustomNumber DESC
    LIMIT 3;

-- SBQ-4
SELECT 
    address2
FROM
    address
WHERE
    address2 IS NOT NULL AND address2 <> ''
    ORDER BY address2 ASC;
    
-- SBQ_5
SELECT 
    A.first_name,
    A.last_name,
    F.title,
    F.release_year
FROM
    actor AS A
        JOIN
    film_actor AS FA ON A.actor_id = FA.actor_id
        JOIN
    film AS F ON FA.film_id = F.film_id
WHERE
    F.description LIKE '%Crocodile%'
        OR F.description LIKE '%Shark%'
        ORDER BY A.last_name;
        
-- SBQ_6
SELECT 
    C.name, COUNT(FC.film_id)
FROM
    film_category AS FC
        JOIN
    category AS C ON C.category_id = FC.category_id
GROUP BY FC.category_id
HAVING COUNT(*) BETWEEN '55' AND '65';

-- SBQ_7
SELECT 
    C.customer_id, C.first_name, C.last_name
FROM
    customer AS C
WHERE
    C.first_name IN (SELECT 
            first_name
        FROM
            actor AS A
        WHERE
            A.actor_id = 8) 
UNION ALL SELECT 
    AC.actor_id, AC.first_name, AC.last_name
FROM
    actor AS AC
WHERE
    AC.first_name IN (SELECT 
            first_name
        FROM
            actor AS A
        WHERE
            A.actor_id = 8);

-- SBQ_8
SELECT 
	ST.store_id,
    SUM(P.amount),
    AVG(P.amount),
    MONTH(P.payment_date) AS P_MONTH,
    YEAR(P.payment_date) AS P_YEAR
FROM
    payment AS P
        LEFT JOIN
    staff AS ST ON P.staff_id = ST.staff_id
    GROUP BY store_id, P_MONTH, P_YEAR
    ORDER BY P_MONTH, P_YEAR;

-- SBQ_9
SELECT 
    R.customer_id,
    C.first_name,
    C.last_name,
    COUNT(R.customer_id) AS M_COUNT
FROM
    rental AS R
        JOIN
    customer AS C ON R.customer_id = C.customer_id
GROUP BY R.customer_id
ORDER BY M_COUNT DESC
LIMIT 3;

