CREATE DATABASE HospitalRecords;
	show tables;
Create table AnestProcedures (proc_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, anest_name VARCHAR(30), start_time , end_time);
INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('1',"Albert",'08:00','11:00');
INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('2',"Albert",'09:00','13:00');
INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('3',"Kamal",'08:00','13:30');                         INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('4',"Kamal",'09:00','15:30');
INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('5',"Kamal",'10:00','11:30');
INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('6',"Kamal",'12:30','13:30');
INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('7',"Kamal",'13:30','14:30');
INSERT INTO 'AnestProcedures' ('proc_id','anest_name','start_time','end_time') VALUES ('8',"Kamal",'18:30','19:00');


USE HospitalRecords;
CREATE VIEW procedures (procedure_id, compare_second_procedure, total) As SELECT P1.proc_id, P2.proc_id, COUNT(*) FROM AnestProcedures As P1, AnestProcedures As P2, AnestProcedures As P3
WHERE P2.Anest_name = P1.Anest_name
AND P3.Anest_name = P1.Anest_name
AND P1.start_time <= P2.end_time
AND P2.start_time < P1.end_time
AND P3.start_time <= P2.end_time
AND P2.start_time < P3.end_time
GROUP BY P1.proc_id, P2.proc_id;

SELECT procedure_id As proc_id, MAX(total) As max_inst_count FROM procedures GROUP BY procedure_id;
