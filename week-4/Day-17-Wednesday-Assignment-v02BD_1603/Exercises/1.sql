create trigger date_check before insert on FiscalYearTable for each row
begin
if new.start_date > new.end_date then signal sqlstate '45000'
SET MESSAGE_TEXT = "Starting date is bigger than end date please fix";
end if;
end;
