CREATE DATABASE PatientsClaimsDB;
Use PatientsClaimsDB;
CREATE Table Claims (claim_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, patient_name VARCHAR(30));
INSERT INTO Claims VALUES (1,"Bassem Dghaidi");
INSERT INTO Claims VALUES (2,"Omar Breidi");
INSERT INTO Claims VALUES (3,"Marwan Sawwan");
CREATE Table Defendants (claim_id INT NOT NULL, defendant_name VARCHAR(30));
INSERT INTO  Defendants  VALUES ( 1,"Jean Skaff");
INSERT INTO  Defendants  VALUES ( 1,"Elie Meouchi");
INSERT INTO  Defendants  VALUES ( 1,"Radwan Sameh");
INSERT INTO  Defendants  VALUES ( 2,"Josef Eid");
INSERT INTO  Defendants  VALUES ( 2,"Paul Syoufi");
INSERT INTO  Defendants  VALUES ( 2,"Radwan Sameh");
INSERT INTO  Defendants  VALUES ( 3,"Issam Awwad");
CREATE Table LegalEvents (claim_id INT NOT NULL, defendant_name VARCHAR(30), claim_status VARCHAR(30), change_date Date);
INSERT INTO LegalEvents  VALUES (1, "Jean Skaff",   "AP", '2016-01-01');
INSERT INTO LegalEvents  VALUES (1, "Jean Skaff",   "OR", '2016-02-02');
INSERT INTO LegalEvents  VALUES (1, "Jean Skaff",   "SF", '2016-03-01');
INSERT INTO LegalEvents  VALUES (1, "Jean Skaff",   "CL", '2016-04-01');
INSERT INTO LegalEvents  VALUES (1, "Radwan Sameh", "AP", '2016-01-01');
INSERT INTO LegalEvents  VALUES (1, "Radwan Sameh", "OR", '2016-02-02');
INSERT INTO LegalEvents  VALUES (1, "Radwan Sameh", "SF", '2016-03-01');
INSERT INTO LegalEvents  VALUES (1, "Elie Meouchi", "AP", '2016-01-01');
INSERT INTO LegalEvents  VALUES (1, "Elie Meouchi", "OR", '2016-02-02');
INSERT INTO LegalEvents  VALUES (2, "Radwan Sameh", "AP", '2016-01-01');
INSERT INTO LegalEvents  VALUES (2, "Radwan Sameh", "OR", '2016-02-01');
INSERT INTO LegalEvents  VALUES (2, "Paul Syoufi",  "AP", '2016-01-01');
INSERT INTO LegalEvents  VALUES (3, "Issam Awwad",  "AP", '2016-01-01');
CREATE Table ClaimStatusCodes (claim_status VARCHAR(30), claim_status_desc VARCHAR(30), claim_seq INT NOT NULL);
INSERT INTO ClaimStatusCodes VALUES ("AP", "Awaiting review panel",  1);
INSERT INTO ClaimStatusCodes VALUES ("OR", "Panel opinion rendered", 2);
INSERT INTO ClaimStatusCodes VALUES ("SF", "Suit filed",             3);
INSERT INTO ClaimStatusCodes VALUES ("CL", "Closed",                 4);
  
  select C.claim_id,C.patient_name,table3.claim_status 
  FROM Claims As C 
  INNER JOIN 
  (Select claim_id, defendant_name, claim_status 
  	FROM 
  	(SELECT claim_id,defendant_name, claim_status 
  		FROM 
  		(SELECT * FROM LegalEvents ORDER BY change_date DESC) As table1
  		 GROUP BY defendant_name) As table2 GROUP BY claim_id) As table3 
  ON C.claim_id=table3.claim_id;


  /*SELECT claim_id,defendant_name, claim_status FROM LegalEvents GROUP BY defendant_name ORDER BY claim_id;
+----------+----------------+--------------+
| claim_id | defendant_name | claim_status |
+----------+----------------+--------------+
|        1 | Radwan Sameh   | AP           |
|        1 | Elie Meouchi   | AP           |
|        1 | Jean Skaff     | AP           |
|        2 | Paul Syoufi    | AP           |
|        3 | Issam Awwad    | AP           |
+----------+----------------+--------------+
5 rows in set (0.00 sec)


    SELECT claim_id,defendant_name, claim_status FROM (SELECT * FROM LegalEvents ORDED BY index DESC) GROUP BY defendant_name ORDER BY claim_id;

    SELECT claim_id,defendant_name, claim_status FROM (SELECT * FROM LegalEvents ORDER BY change_date DESC) As table1 GROUP BY defendant_name ORDER BY claim_id;
    +----------+----------------+--------------+
| claim_id | defendant_name | claim_status |
+----------+----------------+--------------+
|        1 | Radwan Sameh   | SF           |
|        1 | Elie Meouchi   | OR           |
|        1 | Jean Skaff     | CL           |
|        2 | Paul Syoufi    | AP           |
|        3 | Issam Awwad    | AP           |
+----------+----------------+--------------+


mysql> Select claim_id, defendant_name, claim_status FROM (SELECT claim_id,defendant_name, claim_status FROM (SELECT * FROM LegalEvents ORDER BY change_date DESC) As table1 GROUP BY defendant_name ORDER BY claim_id) As table2 GROUP BY claim_id;
+----------+----------------+--------------+
| claim_id | defendant_name | claim_status |
+----------+----------------+--------------+
|        1 | Elie Meouchi   | OR           |
|        2 | Paul Syoufi    | AP           |
|        3 | Issam Awwad    | AP           |
+----------+----------------+--------------+
3 rows in set (0.00 sec)


select C.claim_id,C.patient_name,claim_status FROM Claims As C INNER JOIN (Select claim_id, defendant_name, claim_status FROM (SELECT claim_id,defendant_name, claim_status FROM (SELECT * FROM LegalEvents ORDER BY change_date DESC) As table1 GROUP BY defendant_name ORDER BY claim_id) As table2 GROUP BY claim_id) As table3 ON C.claim_id=table3.claim_id;
+----------+----------------+--------------+
| claim_id | patient_name   | claim_status |
+----------+----------------+--------------+
|        1 | Bassem Dghaidi | CL           |
|        2 | Omar Breidi    | AP           |
|        3 | Marwan Sawwan  | AP           |
+----------+----------------+--------------+
3 rows in set (0.00 sec)


select C.claim_id,C.patient_name,table3.claim_status FROM Claims As C INNER JOIN (Select claim_id, defendant_name, claim_status FROM (SELECT claim_id,defendant_name, claim_status FROM (SELECT * FROM LegalEvents ORDER BY change_date DESC) As table1 GROUP BY defendant_name) As table2 GROUP BY claim_id) As table3 ON C.claim_id=table3.claim_id;
+----------+----------------+--------------+
| claim_id | patient_name   | claim_status |
+----------+----------------+--------------+
|        1 | Bassem Dghaidi | OR           |
|        2 | Omar Breidi    | AP           |
|        3 | Marwan Sawwan  | AP           |
+----------+----------------+--------------+
3 rows in set (0.00 sec)

