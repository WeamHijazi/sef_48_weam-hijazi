	var moveInfo;
    var moveInc = 1;
    var callStack;

    var barsInfo = [{}, {}, {}];

    var diskPosTop, diskPosLeft, DiskID;

var hanoi = function(disc,src,aux,dst){
	if(disc>0){
		hanoi(disc-1,src,dst,aux);
		document.write("Move disc "+disc+" from "+src+" to "+dst+ "<br />");
		//document.write("<canvas id="myCanvas" width="50" height="10"></canvas>"	);
		hanoi(disc-1, aux,src,dst);
	}
};

  window.onload = function ()
    {   diskPosTop = new Array();
        diskPosLeft = new Array();
        DiskID = [disk0, disk1, disk2, disk3, disk4]
        for (var i = 0; i < 5; i++)
        {  diskPosTop[i] = DiskID[i].style.top; 
           diskPosLeft[i] = DiskID[i].style.left; } 

    };


 function moveDisk() {
        if (callStack.length == 0) return;

        var param = callStack.shift();  // Get call parameters from callStack
        // Note: throughout the code, I use fromBar, toBar to refer to towers
        var  fromBar = param[0];
        var toBar = param[1];

        var elem = document.getElementById(barsInfo[fromBar].disks.pop());  // find top elemnet in fromBar

        moveInfo = { elem: elem,
            fromBar: fromBar,
            toBar: toBar,
            whichPos: "top", // element position property for movement
            dir: -1,  // 1 or -1
            state: "up", // move upward
            endPos: 60    // end position (in pixels) for move upward
        }

    
    };

      function executeHanoi()
     {
    

        // Move Disks to start column  
        for (var i = 0; i < 5; i++)
        {  DiskID[i].style.top = diskPosTop[i];
           DiskID[i].style.left= diskPosLeft[i];
        } 
  
        barsInfo[0].disks = ['disk0', 'disk1', 'disk2', 'disk3', 'disk4'];
        //alert(barsInfo[0].disks.pop().id);
        barsInfo[1].disks = [];
        barsInfo[2].disks = [];
      

       

        callStack = [];  // callStack array is global

        Hanoi(5, 0, 2, 1);

        moveDisk(); // moveDisk takes its parameters from callStack
    };

    function animateMove() {
        var elem = moveInfo.elem;
        var dir = moveInfo.dir;

        var pos = parseInt(elem[(moveInfo.whichPos == "left") ? "offsetLeft" : "offsetTop"]);

        if (((dir == 1) && (pos >= moveInfo.endPos)) || ((dir == -1) && (pos <= moveInfo.endPos))) {  // alert(moveInfo.state); 
            if (moveInfo.state == "up") {
                moveInfo.state = "hor";
                moveInfo.whichPos = "left";
                moveInfo.dir = 1;
                if (moveInfo.fromBar > moveInfo.toBar) moveInfo.dir = -1;
                //alert("toBar:" + moveInfo.toBar);
                var toBar = document.getElementById("bar" + moveInfo.toBar);
                // Next line: 15px is half of tower width    
                moveInfo.endPos = toBar.offsetLeft - Math.floor(elem.offsetWidth / 2) + 15;
                return;
            }

            else if (moveInfo.state == "hor") // move down
            {
                moveInfo.state = "down";
                moveInfo.whichPos = "top";
                moveInfo.dir = 1;
                //alert(elem.offsetHeight);
                moveInfo.endPos = document.getElementById("bottombar").offsetTop - (barsInfo[moveInfo.toBar].disks.length + 1) * elem.offsetHeight;
                return;
            }

            else // end of current call to moveDisk, issue next call
            {
              
                barsInfo[moveInfo.toBar].disks.push(elem.id);
                moveDisk();
                return;
            }
        }


        // Move Disk
        pos = pos + dir * moveInc;
        elem.style[moveInfo.whichPos] = pos + "px";
    };