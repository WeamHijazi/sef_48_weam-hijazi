<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ url ('/css/app.css') }}" rel="stylesheet">
    <link href="{{ url ('/css/login.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            
            <div id="instaImage" >
                <a  href="{{ url('/home') }}">
                    <img src="{!! asset('assets/img/logo.png') !!}" alt="insta-logo">
                </a>
            </div>

            <div class="collapse navbar-collapse navWidth" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Sign Up</a></li>
                    @else
                    <div id="whenLoggedIn" >
                        <div class="Search">
                            <input class="SearchInsta" type="text" placeholder="Search" value=""></input>
                        </div>
                        <div id="upload-img">
                            <a href="{{ url('/createposts') }}">
                                <img src="{!! asset('assets/img/upload.jpeg') !!}">
                            </a>
                        </div>
                        <div class="rightIcons">
                            <div id="likes">
                                <a href="#">
                                    <img src="{!! asset('assets/img/2.jpeg') !!}">
                                </a>
                            </div>
                            <li class="dropdown">
                                <a href="{{ url('/profile/{user()->name}') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <div id="person">
                                        <img src="{!! asset('assets/img/1.jpeg') !!}">
                                    </div>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                    
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </div>
                    </div>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- Scripts -->
    <script src="{{ url('/js/app.js') }}"></script>
</body>
</html>
