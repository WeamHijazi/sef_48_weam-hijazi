@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
             @foreach($posts as $post)
                <div >{!! $post->user->name!!}</div>
                   <img src="{!! url('/uploads/'.$post['url_image']) !!}" width="100%" >
                   <div class="panel-heading"> {!! $post['url_image']!!}
                        <div class="panel-body">
				            <div>
						          {!! $post['caption']!!}<br>
						           <a href="blogpost/{!! $post['id_post']!!}"></a>
						           <form method="POST" role="form"   action="{{ url('/comment/{!! $post['id_post']!!}') }}">
                                        <input  type=text name="comment{!! $post['id_post']!!}">
                                        <input type="submit" >
                                    </form>
					        </div>
					    </div>
					</div>
			 @endforeach
               
            </div>
        </div>
    </div>
</div>
@endsection
