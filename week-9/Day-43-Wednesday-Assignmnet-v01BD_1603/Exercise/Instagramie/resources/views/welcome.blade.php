<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Instagramie</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000;
                font-family: 'Raleway';
                font-weight: 100;
                height: 100vh;
                margin: 0;

            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #0000FF;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #logo {
                
                height: 50%;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Sign Up</a>
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Instagramie
                </div>
                
                <div id="logo">
                <img src="{!! asset('assets/img/instagram-logo.png') !!}" alt="insta-logo">
                </div>

                <div class="links">
                    <a href="https://www.instagram.com/about/us/">ABOUT US</a>
                    <a href="http://blog.instagram.com/">BLOG</a>
                    <a href="https://help.instagram.com/">SUPPORT</a>
                    <a href="https://help.instagram.com/478745558852511">TERMS</a>
                    <a href="https://www.instagram.com/developer/">API</a>
                </div>
            </div>
        </div>
    </body>
</html>
