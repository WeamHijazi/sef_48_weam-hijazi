@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div id="Poststyle" class="panel-heading">Add A New Photo</div>
                <div class="panel-body">
                    <form  method="POST" role="form"  enctype="multipart/form-data" action="{{url('/createposts') }}">
                        {{ csrf_field() }}
                         <b>Go To Gallery:</b>
                        <input type="file" name="image" id="image"><br>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label"></label>
                             <div class="col-md-8 caption">
                                <textarea class="form-control" name="caption" placeholder="write a caption ...">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <br>
                                <button type="submit" class="btn btn-primary share" >share</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection