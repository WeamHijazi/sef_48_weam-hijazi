<?php

namespace Instagramie\Http\Controllers;

use Illuminate\Http\Request;
use Instagramie\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'DESC')->get();
        return view('home')->with('posts',$posts);
        //return view('home');
    }

    public function ViewAllPosts()
    {
        $posts = Post::all();
        return view('ViewUserPosts')->with('posts',$posts);

    }


     public function CreatePosts(Request $request) 
    {
         $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg',]);
        $image = $request->file('image');
        $input['imagename'] = time().'_'.$request->user()->id.'_'.$image->getClientOriginalExtension();
        $destinationPath = public_path('uploads');
        $image->move($destinationPath, $input['imagename']);
        $newPost = new Post();
        $newPost->caption = $request->get('caption');
        $newPost->user_id = $request->user()->id;
        $newPost->img_url = $input['imagename'];
        $newPost->save();
        //echo "trial is trial";
     return redirect("home");
        
    }
        
    public function Post()
    {
        return view('Posts');
    }
       

       public function ViewUserPosts($name)
       {
        $posts = Post::orderBy('created_at', 'DESC')->where('user_id',$name)->get();
        return view('ViewUserPosts')->with('posts',$posts);
       }
    
        

}
